<?php
include "actions/connection.php";
$query = "SELECT * FROM lists";
$result = $conn->query($query);

?>

<html lang="en">

<?php include "includes/header.php";?>

<body>
   
<div class="container py-5">

  <!-- For demo purpose -->
  <div class="row text-center text-white mb-5">
  <div class="container pt-4">
        <header>
        <h1 class="display-4">To do list creator</h1>
                    <p class="font-italic mb-1">Make your own amazing todolist here!</p>
                    <p class="font-italic"> Created By
                        <a class="text- text-white">
                            <u>Yavuz Dereli</u>
                        </a>
                    </p>
                   <?php include "includes/pages.php";?>
        </header>
    </div>
  </div>
  <!-- End -->

    <div class="row">
        <div class="col-lg-8 mx-auto">
            <?php while($row = $result->fetch(PDO::FETCH_ASSOC)){?>
            
                <!-- List group-->
                <ul class="list-group shadow">
                    <!-- list group item-->
                    <?php if(count($row) > 0){?>
                    <li class="list-group-item ">
                    <!-- Custom content-->
                    <div class="media align-items-lg-center flex-column flex-lg-row p-3">
                        <div class="media-body order-2 order-lg-1">
                            <h5 class="mt-0 font-weight-bold mb-2">List Name: <?php
                            $list_id = $row['list_id']; 
                            echo $row["list_name"];
                            
                            ?></h5>
                            <p class="font-italic text-muted mb-0 small text-bold">Tasks selected: <?php 
                            $querys = "SELECT * FROM tasks";
                            $results = $conn->query($querys);
                            while($rows = $results->fetch(PDO::FETCH_ASSOC)){
                                if($list_id == $rows['list_id']){
                                    echo $rows['task_name']; echo " ";
                                }
                            }

                            
                             
                            
                            ?></p>
                            <div class="d-flex align-items-center justify-content-between mt-1">
                                <ul class="list-inline m-0">
                                    <li class="list-inline-item">
                                        <a class="btn btn-success btn-sm rounded-0" type="button" href="editlists.php?id=<?php echo $row['list_id']; ?>"  title="Edit"><i class="fa fa-edit"></i></a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a onclick="return confirm('Are you sure you want to delete <?php echo $row['list_name']; ?>')" href="actions/delete_list.php?id=<?php echo $row['list_id']; ?>" class="btn btn-danger btn-sm rounded-0" type="button"  title="Delete"><i class="fa fa-trash"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- End -->
                    </li>
                        <?php }?>
                    <!-- End -->
            <?php }?>
        </div>
           
    </div>





    <?php include "includes/scripts.php";?>
</body>

</html>