<?php
include "actions/connection.php";


$id = $_GET["id"];
$sql = "SELECT * FROM `lists` WHERE list_id = $id";
$result = $conn->query($sql);
$row = $result->fetch(PDO::FETCH_ASSOC);

?>

<html lang="en">

<?php include "includes/header.php";?>;

<body>
    <section class="py-5 header text-center text-white">
            <div class="container pt-4">
                <header>
                    <h1 class="display-4">To do list creator</h1>
                    <p class="font-italic mb-1">Make your own amazing todolist here!</p>
                    <p class="font-italic"> Created By
                        <a class="text- text-white">
                            <u>Yavuz Dereli</u>
                        </a>
                    </p>
                    <?php include "includes/pages.php";?>
                </header>
            </div>
        </section>


    <div class="container">
        <script src="//code.jquery.com/jquery-3.1.0.slim.min.js"></script>
        <div class="row">
            <div class="col-md-12">
                <div class="main-todo-input-wrap">
                    <div class="main-todo-input fl-wrap">
                        <form  method="post" action="actions/edit_list.php" name="edit_list">
                        <div class="main-todo-input-item"> 
                        <input style="display:none;"  name="list_id" value="<?php echo $id;?>">
                        <input type="text" id="todo-list-item" name="list_name"
                                value="<?php echo $row["list_name"];?>"> </div> <button name="edit_list" type="submit"
                            class="add-items main-search-button">Save</button>
                        </form>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>



    <?php include "includes/scripts.php";?>
</body>

</html>