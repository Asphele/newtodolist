<html lang="en">


<?php include "includes/header.php";?>

<body>
    <section class="py-5 header text-center text-white">
        <div class="container pt-4">
            <header>
                <h1 class="display-4">To do list creator</h1>
                <p class="font-italic mb-1">Make your own amazing todolist here!</p>
                <p class="font-italic"> Created By
                    <a class="text- text-white">
                        <u>Yavuz Dereli</u>
                    </a>
                </p>
                <?php include "includes/pages.php";?>
            </header>
        </div>
    </section>

    <div class="container">

        <script src="//code.jquery.com/jquery-3.1.0.slim.min.js"></script>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 mx-auto">
                        <div class="card shadow border-0 mb-5">
                            <div class="card-body">
                                <h2 class="h4 mb-1 text-center">Make your own Task</h2>
                                <p class="small text-muted font-italic mb-4 text-center">What kind of wonderous tasks will you have?
                                </p>
                                <div class="row">
                                    <div class="col">
                                        <div class="main-todo-input-wrap d-flex justify-content-center">
                                            <div class="main-todo-input fl-wrap m-2">
                                                <form method="post" action="actions/create.php" name="create_task">
                                                    <div class="form-group row">
                                                        <label for="inputEmail3" class="col-sm-5 col-form-label">Task
                                                            Name:</label>
                                                        <div class="col-sm-10">
                                                            <input name="task_name" type="text" class="form-control"
                                                                id="inputEmail3" placeholder="Sleeping">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="inputPassword3"
                                                            class="col-sm-5 col-form-label">Time:</label>
                                                        <div class="col-sm-10">
                                                            <input name="task_time" type="time" class="form-control"
                                                                id="inputPassword3" placeholder="Password">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-sm-5 col-form-label">Status:</div>
                                                        <div class="col-sm-10">
                                                            <div>
                                                                <select name="task_status" class="form-control"
                                                                    id="exampleFormControlSelect1">
                                                                    <option>Planned</option>
                                                                    <option>In Progress</option>
                                                                    <option>Finished</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-sm-10">
                                                            <button type="submit"
                                                                class="btn btn-primary">Create</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        
    </div>





    <?php include "includes/scripts.php";?>
</body>

</html>