<?php
include "connection.php";

$selectedTasks = array();
$nameList = $_POST["list_name"];


$checked_count = count($_POST['tasks']);

// Loop to store and display values of individual checked checkbox.
foreach($_POST['tasks'] as $selected) {
    array_push($selectedTasks, $selected);
}


// Creating the list before updating the tasks
createList($conn, $nameList, $selectedTasks);

function createList($conn, $nameList, $selectedTasks){
    $stmt = $conn->prepare("INSERT INTO lists(list_name) values (:name)");
        $stmt->bindParam(':name', $nameList);
        $stmt->execute();
        $getId = $conn->lastInsertId();

        // Updating tasks since list has been created
        updateTasks($conn, $nameList, $selectedTasks, $getId);

}

function updateTasks($conn, $nameList, $selectedTasks, $getId){
    for($i = 0; $i < count($selectedTasks); $i++){
        $task = $selectedTasks[$i];
       
        // Looking for tasks so it can be updated
        $query = "SELECT * FROM tasks WHERE task_name= '$task'";
        $result = $conn->query($query);
        $row = $result->fetch(PDO::FETCH_ASSOC);
    
        // Getting the task id so we can locate which tasks, list_id needs to be updated
        $id = $row['task_id'];

        

        // Updating the tasks_id to the list_id
        $stmt = $conn->prepare("UPDATE tasks SET list_id='$getId' WHERE task_id=$id");
        $stmt->bindParam(':name', $name, PDO::PARAM_STR, 255);
        $stmt->execute();
           
    }
}


    
header("Location: ../index.php");
?>