<?php
include "actions/connection.php";
$query = "SELECT * FROM tasks";
$result = $conn->query($query);


?>

<html lang="en">

<?php include "includes/header.php";?>

<body>
   

<section class="py-5 header text-center text-white">
    <div class="container pt-4">
        <header>
        <h1 class="display-4">To do list creator</h1>
                    <p class="font-italic mb-1">Make your own amazing todolist here!</p>
                    <p class="font-italic"> Created By
                        <a class="text- text-white">
                            <u>Yavuz Dereli</u>
                        </a>
                    </p>
                    <?php include "includes/pages.php";?>
        </header>
    </div>
</section>


<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7 mx-auto">
                <div class="card shadow border-0 mb-5">
                    <div class="card-body p-5">
                        <h2 class="h4 mb-1">Make your own list</h2>
                        <p class="small text-muted font-italic mb-4">Write a name for your list and choose from the below tasks to be put in your own list.</p>

                        <form action="actions/createList.php" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" name="list_name" placeholder="Enter your list name">
                            </div>

                            <ul class="list-group m-2">
                                <?php while($row = $result->fetch(PDO::FETCH_ASSOC)){?>
                                    <li class="list-group-item rounded-0">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input boxex" name="tasks[]" value="<?php echo $row["task_name"];?>" type="checkbox" id="inlineCheckbox2">
                                            <label class="form-check-label" for="inlineCheckbox2"><?php echo $row['task_name'];?></label>
                                        </div>
                                    </li>
                                <?php }?>
                            </ul>
                            <button class="btn btn-primary btn-lg btn-block" type="submit">Create</button>
                        </div>
                       
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>





    <?php include "includes/scripts.php";?>
</body>

</html>