function sort_tasks(){
    var liContents = [];
    $('ul li').each(function() {
        liContents.push($(this).html().toLowerCase());
    });
    liContents.sort(liSorter);
    $('ul li').each(function() {
        $(this).html(liContents.pop());
    });

}

function sort_tasks_time(){
  var liContents = [];
  $('ul li').each(function() {
      liContents.push($(this).html());
  });
  liContents.sort(liSorter_time);
  $('ul li').each(function() {
      $(this).html(liContents.pop());
  });



  // let times = [];
  // $(".Date").each(function(){
  //   times.push($(".Date").text());
  // })


}

function liSorter(a, b) {
    if(b < a) { return -1; }
    if(b > a) { return 1; }
    return 0;
}

function liSorter_time(a, b) {
  if(a < b) { return -1; }
  if(a > b) { return 1; }
  return 0;
}


$(document).ready(function(){
    // Filter system
    $("#myInput").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#list-items li").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
  });