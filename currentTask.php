<?php
include "actions/connection.php";
$query = "SELECT * FROM tasks";
$result = $conn->query($query);
?>

<html lang="en">


<?php include "includes/header.php";?>

<body>
    <section class="py-5 header text-center text-white">
        <div class="container pt-4">
            <header>
                <h1 class="display-4">To do list creator</h1>
                <p class="font-italic mb-1">Make your own amazing todolist here!</p>
                <p class="font-italic"> Created By
                    <a class="text- text-white">
                        <u>Yavuz Dereli</u>
                    </a>
                </p>
                <?php include "includes/pages.php";?>
            </header>
        </div>
    </section>

    <div class="container">

        <script src="//code.jquery.com/jquery-3.1.0.slim.min.js"></script>
       

        </form>
        <div class="row">
            <div class="col-md-12">
                <div class="main-todo-input-wrap">
                    <div class="main-todo-input fl-wrap todo-listing">
                        <div class="centerIT">
                        <button class onclick="sort_tasks();">Sort Tasks by name</button> <button class onclick="sort_tasks_time()">Sort Tasks by time</button> <input type="text" id="myInput" placeholder="Search Task">
                        </div>
                        <form method="post" name="delete_task">
                            <ul id="list-items">
                                <?php while($row = $result->fetch(PDO::FETCH_ASSOC)){?>
                                <li class="items">
                                    Name: <?php echo $row['task_name'];?>
                                    Time: <span class="Date" value="<?php echo $row['task_time'];?>"><?php echo $row['task_time'];?></span>
                                    <a onclick="return confirm('Are you sure you want to delete <?php echo $row['task_name']; ?>')"
                                        href="actions/delete.php?id=<?php echo $row['task_id']; ?>"><i
                                            class="fa fa-trash button delete_button" aria-hidden="true"></i></a>
                                    <a href="editmodal.php?id=<?php echo $row['task_id']; ?>"><i
                                            class="fa fa-wrench button edit_button" aria-hidden="true"></i></a>
                                </li>
                                <?php }?>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <?php include "includes/scripts.php";?>
</body>

</html>